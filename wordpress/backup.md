## Check the docker status

```
sudo docker ps
sudo docker exec -it dc2 sh
sudo docker stop dc2
sudo docker start dc2
```

## after you get into docker file, you can use following command to install tools and get zip files

```
apt update
apt install zip
apt install unzip
apt install telnetd
scp file.txt user@192.168.1.154:/path/to/destination/
unzip html.zip
unzip html.zip -d /path/to/destination/
cp -Rf . /path/to/destination/
cp -Rfp . /path/to/destination/
scp user@192.168.1.154:/home/junwang/wordpress/html.zip .
chmod 755 /path/to/directory

```

## back up and restore SQL file

Let's write this next time. 
